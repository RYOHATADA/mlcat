import os
import sys
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from mlcat.classifier_knn import KNN
from mlcat.classifier_rf import RandomForest
from mlcat.classifier_sv import SupportVector



class Clscat:


    def __init__(self, datasetpath, target, testsize, method, tuner, outdir, tag, dropcolumn = None):

        self.datasetpath = datasetpath
        self.target = target
        self.testsize = testsize
        self.method = method
        self.tuner = tuner
        self.outdir = outdir
        self.tag = tag
        self.dropcolumn = dropcolumn


    # パラメーターのチェック #
    def param_check(self):

        pass


    # Outdirがなければ作成 #
    def outdir_check(self):

        try:

            os.mkdir(self.outdir)

        except:

            pass

    # データのチェックと前処理 #
    def data_check(self):

        try:

            dataset_org = pd.read_csv(self.datasetpath,sep=",")

        except:

            print("Clscat Error : Can't read filename, please check it")
            sys.exit()

        dataset_org = dataset_org.astype(float)

        try:

            y = dataset_org[self.target].to_numpy()

        except:

            print("Clscat Error : Can't find target value '" + str(self.target) + "', please check dataset")
            sys.exit()


        if self.dropcolumn == None:

            x = dataset_org.drop(self.target, axis=1).to_numpy()

        else:

            try:

                x = dataset_org.drop(self.target, axis=1).drop(self.dropcolumn, axis=1).to_numpy()

            except:

                print("Clscat Error : Can't find explanation value, please check dataset")
                sys.exit()

        x_train, x_test, self.y_train, self.y_test = train_test_split(x, y, test_size = self.testsize, random_state = 0)

        sc = StandardScaler()
        sc.fit(x_train)
        self.x_train = sc.transform(x_train)
        self.x_test = sc.transform(x_test)


    def run(self):

        print("### Clscat Version 2021-Mar ###")

        # 前処理 #
        self.param_check()
        self.outdir_check()
        self.data_check()

        # モデルインスタンスの動的生成 #
        try:

            cls = globals()[self.method]
            model = cls(self.x_train, self.y_train, self.x_test, self.y_test)

        except:

            print("Clscat Error : Can't find '" + str(self.method) + "', please check method name")
            sys.exit()

        # モデルのチューニング #
        try:

            model.tune(**self.tuner)

        except:

            print("")
            print("Clscat Erorr : Parameter setting doesn't match method, please check 'param_config'")
            sys.exit()

        # モデルの構築 #
        model.build()

        # 予測 #
        model.predict()

        # 結果の保存 #
        model.save_log(self.outdir, self.tag)

        print("### Clscat Job Successfully Done ###")
