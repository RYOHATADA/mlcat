import time
import os
import sys
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from mlcat.regressor_linear import Linear
from mlcat.regressor_sv import SupportVector
from mlcat.regressor_lightgbm import LightGBM
from mlcat.regressor_rf import RandomForest
from mlcat.regressor_nn3 import NN3
from mlcat.regressor_nn5 import NN5


class Regcat:

    def __init__(self, datasetpath, target, testsize, method, tuner, outdir, tag, dropcolumn = None):

        self.datasetpath = datasetpath
        self.target = target
        self.testsize = testsize
        self.method = method
        self.tuner = tuner
        self.outdir = outdir
        self.tag = tag
        self.dropcolumn = dropcolumn


    # パラメーターのチェック #
    def param_check(self):

        if (self.method == "NN3") or (self.method ==  "NN5"):

            if self.tuner["n_jobs"] != 1:

                print("Regcat Error : Can't use multi cpu in neural network regression, please set 'n_jobs = 1'")
                sys.exit()

            if self.tuner["param_config"] != "default":

                for i in self.tuner["param_config"]:

                    if i[0] not in ["optimizer", "activation", "epochs", "batch_size", "learning_rate"]:

                        print("Regcat Error : Neural network must 'optimizer', 'activation', 'batch_size', 'learning_rate' and 'epochs'")
                        sys.exit()


    # Outdirがなければ作成 #
    def outdir_check(self):

        try:

            os.mkdir(self.outdir)

        except:

            pass


    # データのチェックと前処理 #
    def data_check(self):

        try:

            dataset_org = pd.read_csv(self.datasetpath,sep=",")

        except:

            print("Regcat Error : Can't read filename, please check it")
            sys.exit()

        dataset_org = dataset_org.astype(float)

        try:

            y = dataset_org[self.target].to_numpy()

        except:

            print("Regcat Error : Can't find target value '" + str(self.target) + "', please check dataset")
            sys.exit()

        if self.dropcolumn == None:

            x = dataset_org.drop(self.target, axis=1).to_numpy()

        else:

            try:

                x = dataset_org.drop(self.target, axis=1).drop(self.dropcolumn, axis=1).to_numpy()

            except:

                print("Regcat Error : Can't find explanation value, please check dataset")
                sys.exit()

        x_train, x_test, self.y_train, self.y_test = train_test_split(x, y, test_size = self.testsize, random_state = 0)

        sc = StandardScaler()
        sc.fit(x_train)
        self.x_train = sc.transform(x_train)
        self.x_test = sc.transform(x_test)


    def run(self):

        print("### Regcat Version 2021-Mar ###")

        # 前処理 #
        self.param_check()
        self.outdir_check()
        self.data_check()

        # モデルインスタンスの動的生成 #
        try:

            cls = globals()[self.method]
            model = cls(self.x_train, self.y_train, self.x_test, self.y_test)

        except:

            print("Regcat Error : Can't find '" + str(self.method) + "', please check method name")
            sys.exit()

        # Linear以外のモデルをチューニング #
        if self.method != "Linear":

            try:

                model.tune(**self.tuner)

            except:

                print("")
                print("Regcat Erorr : Parameter setting doesn't match method, please check 'param_config'")
                sys.exit()

        # モデルの構築 #
        model.build()

        # 予測 #
        model.predict()

        # 結果の保存 #
        model.save_log(self.outdir, self.tag)

        print("### Regcat Job Successfully Done ###")

