import warnings
warnings.filterwarnings('ignore')
import lightgbm as lgb
from mlcat.base import Base


class LightGBM(Base):

    def __init__(self, x_train, y_train, x_test, y_test):

        super(LightGBM, self).__init__(x_train, y_train, x_test, y_test)

        self.default_params  = [
                               ["learning_rate", "tune", "float", 0.001, 0.1],
                               ["num_leaves", "tune", "int", 10, 300],
                               ["reg_alpha",  "tune", "loguniform", 0.001, 10],
                               ["reg_lambda", "tune", "loguniform", 0.001, 10],
                               ]

        self.problem = "Regression"


    def get_method(self, params):

        method = lgb.LGBMRegressor(**params)

        return method
