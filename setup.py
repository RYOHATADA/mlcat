from setuptools import setup

install_requires = []
packages = ['mlcat']

setup(
     name='mlcat',
     version='1.0.0',
     packages=packages,
     install_requires=install_requires,
     )
