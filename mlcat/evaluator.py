from sklearn.metrics import r2_score, accuracy_score, f1_score, recall_score, precision_score


class Evaluator:

    def __init__(self):

        self.score = None


    # scoreの算出 #
    def calc(self, y_obs, y_pred, metrics):

        if metrics == "r2":

            self.score = r2_score(y_obs, y_pred)

        elif metrics == "accuracy":

            self.score = accuracy_score(y_obs, y_pred)

        elif metrics == "f1":

            self.score = f1_score(y_obs, y_pred)

        elif metrics == "recall":

            self.score = recall_score(y_obs, y_pred)

        elif metrics == "precision":

            self.score = precision_score(y_obs, y_pred)
