from abc import ABCMeta, abstractmethod
from mlcat.tuner import Tuner
from mlcat.evaluator import Evaluator
from mlcat.logger import Logger


class Base(metaclass = ABCMeta):

    def __init__(self, x_train, y_train, x_test, y_test):

        self.x_train = x_train                   # x_train
        self.x_test = x_test                     # x_test
        self.y_train = y_train                   # y_train
        self.y_test = y_test                     # y_test

        self.clsname = self.__class__.__name__   # 自身の名前
        self.y_train_pred = []                   # y_trainの予測値
        self.y_test_pred= []                     # y_testの予測値
        self.metrics = None                      # メトリクス
        self.params = {}                         # パラメーター
        self.default_params = []                   # チューニング設定
        self.model = None                        # 構築モデル
        self.nn = False                          # NN系かの確認, オーバーライド必要
        self.problem = None                      # 回帰か分類の確認, オーバーライド必須
        self.history = None                      # NN系のLoss関数の推移
        self.train_score = 0                     # Trainスコア
        self.test_score = 0                      # Testスコア


    # 抽象メソッド #
    @abstractmethod
    def get_method(self, params):

        pass


    # チューニング (cv_method : cv方法, n_cv : cv数, n_trials : 試行回数, n_jobs : cpu並列数) #
    def tune(self, param_config, cv_method, n_cv, metrics, n_trials, n_jobs):

        self.metrics = metrics

        # Tunerクラスからインスタンス生成 #
        tuner = Tuner(self.x_train, self.y_train, self.get_method)

        # param_configをデフォルト設定にする場合 #
        if param_config == "default":

           param_config = self.default_params

        tuner.tune(param_config, cv_method, n_cv, metrics, n_trials, n_jobs)
        self.params = tuner.params


    # モデルの構築 #
    def build(self):

        method = self.get_method(self.params)

        self.history = method.fit(self.x_train, self.y_train)
        self.model = method
        self.y_train_pred = method.predict(self.x_train)
        self.train_score = self._get_score(self.y_train, self.y_train_pred, self.metrics)


    # スコアの算出 #
    def _get_score(self, y_obs, y_pred, metrics):

        # Evaluatorクラスからインスタンス生成 #
        evaluator = Evaluator()
        evaluator.calc(y_obs, y_pred, metrics)
        score = evaluator.score

        return score


    # 予測 #
    def predict(self):

        self.y_test_pred = self.model.predict(self.x_test)
        self.test_score = self._get_score(self.y_test, self.y_test_pred, self.metrics)


    # ログの出力 #
    def save_log(self, outdir, tag):

        # Loggerクラスからインスタンス生成 #
        logger = Logger(outdir, tag)

        logger.save_log(self.clsname, self.params, self.train_score, self.test_score)
        logger.save_yy(self.y_test, self.y_test_pred)

        if self.nn == True:

            logger.save_history(self.history)

        if self.problem == "Regression":

            logger.save_yyplot(self.y_test, self.y_test_pred)

        elif self.problem == "Classification":

            logger.save_confmatrix(self.y_test, self.y_test_pred)
