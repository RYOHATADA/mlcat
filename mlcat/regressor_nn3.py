from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Input, Dense, Dropout, BatchNormalization
from tensorflow.keras import optimizers
from tensorflow.keras.wrappers.scikit_learn import KerasRegressor
from tensorflow.keras import backend as K
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
from mlcat.base import Base


class NN3(Base):

    def __init__(self, x_train, y_train, x_test, y_test):

        super(NN3, self).__init__(x_train, y_train, x_test, y_test)

        self.default_params = [
                              ["epochs", "fix", "int", 30],
                              ["batch_size", "fix", "int", 32],
                              ["activation", "fix", "categorical", "relu"],
                              ["optimizer", "fix", "categorical", "Adam"],
                              ["learning_rate", "tune", "float", 0.0005, 0.0015],
                              ]

        self.nn = True
        self.problem = "Regression"


    def get_method(self, params):

        def nn_model():

            regressor = Sequential()
            regressor.add(Dense(64, activation = params["activation"], input_shape=(self.x_train.shape[1],)))
            regressor.add(Dense(64, activation = params["activation"]))
            regressor.add(Dense(1))

            if params["optimizer"] == "Adam" :

                optimizer = optimizers.Adam(lr = params["learning_rate"])

            elif params["optimizer"] == "Adagrad" :

                optimizer = optimizers.Adagrad(lr = params["learning_rate"])

            regressor.compile(optimizer = optimizer,
                              loss = 'mse',
                              metrics = ['mae'])

            return regressor

        method = KerasRegressor(build_fn = nn_model, epochs = params["epochs"], batch_size = params["batch_size"], verbose=0)
        K.clear_session()

        return method
