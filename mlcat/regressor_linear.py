from sklearn import linear_model
from mlcat.base import Base


class Linear(Base):

    def __init__(self, x_train, y_train, x_test, y_test):

        super(Linear, self).__init__(x_train, y_train, x_test, y_test)

        self.default_params = []
        self.problem = "Regression"
        self.metrics = "r2"

    def get_method(self, params):

        method = linear_model.LinearRegression()

        return method
