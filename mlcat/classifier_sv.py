import warnings
warnings.filterwarnings('ignore')
from sklearn.svm import SVC
from mlcat.base import Base

class SupportVector(Base):

    def __init__(self, x_train, y_train, x_test, y_test):

        super(SupportVector, self).__init__(x_train, y_train, x_test, y_test)

        self.default_params = [
                              ["C", "tune", "loguniform", 1e0, 1e2],
                              ["gamma", "tune", "loguniform", 1e-3, 3.0],
                              ]

        self.problem = "Classification"


    def get_method(self, params):

        method = SVC(**params)

        return method
