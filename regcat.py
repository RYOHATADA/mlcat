# Regcat 2021-Mar ver #

import os
from mlcat.regcat import Regcat

# データセット名と出力先、出力ファイル名を指定 #
cd = os.path.dirname(os.path.abspath(__file__))
datasetpath = cd + "/data/chignolin_dataset.csv"
outdir = cd + "/result"
tag = "test"

# 目的変数名とテストサイズ #
# dropcolumnを使う際は、リスト内に説明変数を記載、使わない場合None #
target = "IFIE"
dropcolumn = None
#dropcolumn = ["comdist", "vdwdist"]
testsize = 0.2

# 回帰手法 (Linear, RandomForest, SupportVector, LightGBM, NN3, NN5) #
method = "RandomForest"

# Param Config の設定 ("default"でない時に読まれる) #
# サンプルを以下に作成したのでこんな感じで自由に書いてください #

sample_rf = [
            ["n_estimators", "tune", "int", 10, 100],
            ["max_depth", "fix", "int", 100],
            ]

sample_svr = ["C", "tune", "loguniform", 1e0, 1e2]

sample_gbm = ["n_estimators", "tune", "int", 10, 100]

sample_nn = [
            ["epochs", "fix", "int", 50],
            ["batch_size", "fix", "int", 32],
            ["activation", "fix", "categorical", "relu"],
            ["optimizer", "tune", "categorical", ["Adam", "Adagrad"]],
            ["learning_rate", "tune", "float", 0.0005, 0.0015],
            ]

# Tunerの設定 (NN系ではcpu並列不可) #
tuner = {"n_jobs" : 1,
         "n_trials" : 3,
         "n_cv" : None,
         "cv_method" : "kfold",
         "param_config" : "default",
         "metrics" : "r2",
         }

#
# 実行 #
regcat = Regcat(datasetpath, target, testsize, method, tuner, outdir, tag, dropcolumn)
regcat.run()
