import os
import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.use('Agg')


class Logger:

    def __init__(self, outdir, tag):

        self.outpath = outdir + "/" + tag


    # 基本情報のログファイルを出力 #
    def save_log(self, method_name, param_data, train_score, test_score):

        # Filename #
        filename = self.outpath + ".log"

        # 処理 #
        log = [method_name, param_data, "train score : " + str(train_score), "test score : " + str(test_score)]

        print("")
        print("train score : " + str(train_score))
        print("test score  : " + str(test_score))
        print("")

        with open(filename, "w") as f:

            for item in log:

                f.write("%s\n" % item)


    # NN系のLoss関数の推移を出力 #
    def save_history(self, history):

        # Filename #
        filename = self.outpath + "_history.csv"

        # 処理 #
        history_df = pd.DataFrame(history.history)
        history_df.to_csv(filename)


    # Parity plotの出力 #
    def save_yyplot(self, y_obs, y_pred):

        # Filename #
        filename = self.outpath + ".png"

        # 処理 #
        yvalues = np.concatenate([y_obs.flatten(), y_pred.flatten()])
        ymin, ymax, yrange = np.amin(yvalues), np.amax(yvalues), np.ptp(yvalues)
        fig = plt.figure(figsize=(8, 8))
        plt.scatter(y_obs, y_pred)
        plt.plot([ymin - yrange * 0.01, ymax + yrange * 0.01], [ymin - yrange * 0.01, ymax + yrange * 0.01])
        plt.xlim(ymin - yrange * 0.01, ymax + yrange * 0.01)
        plt.ylim(ymin - yrange * 0.01, ymax + yrange * 0.01)
        plt.xlabel('y_observed', fontsize=24)
        plt.ylabel('y_predicted', fontsize=24)
        plt.title('Observed-Predicted Plot', fontsize=24)
        plt.tick_params(labelsize=16)
        fig.savefig(filename)

    # yの実測値と予測値を出力 #
    def save_yy(self, y_obs, y_pred):

        # Filename #
        filename = self.outpath + "_yy.csv"

        # 処理 #
        df = pd.DataFrame(y_obs, columns = ["observed"])
        df["predicted"] = y_pred
        df.to_csv(filename)

    # 混同行列の算出 #
    def save_confmatrix(self, y_obs, y_pred):

        # Filename #
        filename = self.outpath + "_cm.csv"

        # 処理 #
        cm =confusion_matrix(y_obs, y_pred)
        df = pd.DataFrame(cm)
        df.to_csv(filename)
