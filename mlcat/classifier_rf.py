import warnings
warnings.filterwarnings('ignore')
from sklearn.ensemble import RandomForestClassifier
from mlcat.base import Base

class RandomForest(Base):

    def __init__(self, x_train, y_train, x_test, y_test):

        super(RandomForest, self).__init__(x_train, y_train, x_test, y_test)

        self.default_params = [
                              ["max_depth", "tune", "int", 10, 100],
                              ["n_estimators", "tune", "int", 10, 100]
                              ]

        self.problem = "Classification"


    def get_method(self, params):

        method = RandomForestClassifier(**params)

        return method
