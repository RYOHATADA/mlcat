import warnings
warnings.filterwarnings('ignore')
from sklearn.neighbors import KNeighborsClassifier
from mlcat.base import Base

class KNN(Base):

    def __init__(self, x_train, y_train, x_test, y_test):

        super(KNN, self).__init__(x_train, y_train, x_test, y_test)

        self.default_params = [
                              ["n_neighbors", "tune", "int", 3, 10],
                              ]

        self.problem = "Classification"


    def get_method(self, params):

        method = KNeighborsClassifier(**params)

        return method
