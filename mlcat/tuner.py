from sklearn.model_selection import KFold, cross_validate, StratifiedKFold
import optuna
from mlcat.evaluator import Evaluator

class Tuner:

    def __init__(self, x_train, y_train, get_method):

        self.x_train = x_train
        self.y_train = y_train
        self.get_method = get_method
        self.params = {}
        self.cv_method = None
        self.n_cv = None
        self.metrics = None


    # チューニング #
    def tune(self, param_config, cv_method, n_cv, metrics, n_trials, n_jobs):

        # loss_funcが使うパラメーター #
        self.param_config = param_config
        self.cv_method = cv_method
        self.n_cv = n_cv
        self.metrics = metrics

        # optunaによるチューニング、loss_funcの呼び出し #
        study = optuna.create_study()
        study.optimize(self.loss_func, n_trials = n_trials, n_jobs = n_jobs)

        # チューニング前後のparamsを統合(Fixのため) #
        self.params.update(study.best_params)


    # 損失関数 #
    def loss_func(self, trial):

        # param_configをチェック、正式なparamsを作成 #
        params = {}

        for i in range(len(self.param_config)):

            if isinstance(self.param_config[0], list) == False:

                param = self.param_config

            else:

                param = self.param_config[i]

            param_name = param[0]
            tune_type = param[1]
            param_type = param[2]

            if tune_type == "fix":

                params[param_name] = param[3]

            elif tune_type == "tune":

                if param_type == "int":

                    params[param_name] = trial.suggest_int(param_name, param[3], param[4])

                elif param_type == "float":

                    params[param_name] = trial.suggest_float(param_name, param[3], param[4])

                elif param_type == "uniform":

                    params[param_name] = trial.suggest_uniform(param_name, param[3], param[4])

                elif param_type == "loguniform":

                    params[param_name] = trial.suggest_loguniform(param_name, param[3], param[4])

                elif param_type == "categorical":

                    params[param_name] = trial.suggest_categorical(param_name, param[3])

        # チューニング前のparamsを保存(Fixのパラメータのため) #
        self.params = params

        # training scoreの算出 #
        method = self.get_method(self.params)

        # cvありの場合 #
        if self.n_cv != None:

            if self.cv_method == "kfold":

                cv = KFold(n_splits = self.n_cv, shuffle = True, random_state = 42)

            elif self.cv_method == "stratifiedkfold":

                cv = StratifiedKFold(n_splits = self.n_cv, shuffle = True, random_state = 42)

            score = cross_validate(method, X = self.x_train, y = self.y_train, scoring = self.metrics, cv = cv)
            score = score["test_score"].mean()

        # cvなしの場合 #
        else:

            method.fit(self.x_train, self.y_train)
            y_train_pred = method.predict(self.x_train)

            # Evaluatorクラスを呼び出し#
            evaluator = Evaluator()
            evaluator.calc(self.y_train, y_train_pred, self.metrics)
            score = evaluator.score

        return 1.0 - score
