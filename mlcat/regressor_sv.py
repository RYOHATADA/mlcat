import warnings
warnings.filterwarnings('ignore')
from sklearn.svm import SVR
from mlcat.base import Base


class SupportVector(Base):

    def __init__(self, x_train, y_train, x_test, y_test):

        super(SupportVector, self).__init__(x_train, y_train, x_test, y_test)

        self.default_params = [
                              ["C", "tune", "loguniform", 1e0, 1e2],
                              ["epsilon", "tune", "loguniform", 1e-1, 1e1]
                              ]

        self.problem = "Regression"


    def get_method(self, params):

        method = SVR(**params)

        return method
