# Clscat 2021-Mar ver #

import os
from mlcat.clscat import Clscat

# データセット名と出力先、出力ファイル名を指定 #
cd = os.path.dirname(os.path.abspath(__file__))
datasetpath = cd + "/data/cancer_dataset.csv"
outdir = cd + "/result"
tag = "test"

# 目的変数名とテストサイズ #
# dropcolumnを使う際は、リスト内に説明変数を記載、使わない場合None #
target = "target"
dropcolumn = None
#dropcolumn = ["comdist", "vdwdist"]
testsize = 0.2

# 手法 (KNN, RandomForest, SupportVector) #
method = "KNN"

# Param Config の設定 ("default"でない時に読まれる) #
# サンプルを以下に作成したのでこんな感じで自由に書いてください #

sample_knn = ["n_neighbors", "fix", "int", 5]

# Tunerの設定 #
# Scoringはaccuracy, precision, recall, f1 から選択#

tuner = {"n_jobs" : 1,
         "n_trials" : 5,
         "n_cv" : None,
         "cv_method" : "stratifiedkfold",
         "param_config" : "default",
         "metrics" : "accuracy",
         }

# 実行 #
clscat = Clscat(datasetpath, target, testsize, method, tuner, outdir, tag, dropcolumn)
clscat.run()
